import struct
import random, math
import time
from random import randint
import sys, traceback, threading, socket

from RtpPacket import RtpPacket
from VideoStream import VideoStream

# import socket
import pyaudio
import wave
import time

import mss
from PIL import Image
from io import BytesIO

dbug = False

p = pyaudio.PyAudio()

CHUNK = 4092
FORMAT = pyaudio.paInt32
CHANNELS = 1
RATE = 44100

streami = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True)

class ServerHelper:
	SETUP = 'SETUP'
	PLAY = 'PLAY'
	PAUSE_CAM = 'PAUSE_CAM'
	PAUSE_SCN = 'PAUSE_SCN'
	TEARDOWN = 'TEARDOWN'
	CAST = 'CAST'

	INIT = 0
	READY = 1
	PLAYING = 2
	CASTING = 3
	CASTING_AND_PLAYING = 4
	state = INIT

	OK_200 = 0
	FILE_NOT_FOUND_404 = 1
	CON_ERR_500 = 2
	
	mon = {'top': 0, 'left': 0, 'width': 1366, 'height': 768}
	clientInfo = {}
	screenSize = (450, 250)
	casting = False
	cam = False
	def __init__(self, clientInfo):
		self.clientInfo = clientInfo

	def run(self):
		threading.Thread(target=self.recvRtspRequest).start()

	def recvRtspRequest(self):
		"""Receive RTSP request from the client."""
		connSocket = self.clientInfo['rtspSocket'][0]
		while True:
			data = connSocket.recv(256)  ###
			if data:
				print '-'*60 + "\nData received:\n" + '-'*60
				self.processRtspRequest(data)

	def processRtspRequest(self, data):
		"""Process RTSP request sent from the client."""
		# Get the request type
		request = data.split('\n')
		if dbug:
			print request;
		line1 = request[0].split(' ')
		requestType = line1[0]
		# Get the media file name
		filename = line1[1]
		# Get the RTSP sequence number
		seq = request[1].split(' ')

		# Process SETUP request
		if requestType == self.SETUP:
			if self.state == self.INIT:
				# Update state
				print "SETUP Request received\n"

				try:

					self.clientInfo['videoStream'] = VideoStream()
					self.state = self.READY

				except IOError:
					self.replyRtsp(self.FILE_NOT_FOUND_404, seq[1])

				# Generate a randomized RTSP session ID
				self.clientInfo['session'] = randint(100000, 999999)

				# Send RTSP reply
				self.replyRtsp(self.OK_200, seq[0])  #seq[0] the sequenceNum received from Client.py
				if dbug:
					print "sequenceNum is " + seq[0]
				# Get the RTP/UDP port from the last line
				self.clientInfo['rtpPort_cam'] = request[2].split(' ')[3]
				self.clientInfo['rtpPort_sound'] = str( int(request[2].split(' ')[3]) + 1)
				self.clientInfo['rtpPort_scn'] = str( int(request[2].split(' ')[3]) + 2)
				print '-'*60 + "\nrtpPort for cam is :" + self.clientInfo['rtpPort_cam'] + "\n" + '-'*60
				print '-'*60 + "\nrtpPort for sound is :" + self.clientInfo['rtpPort_sound'] + "\n" + '-'*60
				print '-'*60 + "\nrtpPort for screen is :" + self.clientInfo['rtpPort_scn'] + "\n" + '-'*60
				# print "filename is " + filename
		elif requestType == self.CAST:
			if (not self.casting) and self.state != self.INIT: #self.state == self.READY or self.state == self.PLAYING:
				print ('-'*60 + "\nCAST Request Received\n" + '-'*60)
				self.casting = True
				# Create a new socket for RTP/UDP
                                ttl = struct.pack('b', 1)
				self.clientInfo["rtpSocket_scn"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                                self.clientInfo["rtpSocket_scn"].setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

				self.replyRtsp(self.OK_200, seq[0])
				if dbug:
					print ('-'*60 + "\nSequence Number ("+ seq[0] + ")\nReplied to client\n" + '-'*60)

				# Create a new thread and start sending RTP packets
				self.clientInfo['event_scn'] = threading.Event()
				self.clientInfo['worker_scn']= threading.Thread(target=self.sendScnRtp)
				self.clientInfo['worker_scn'].start()
		# Process RESUME request
			elif not self.casting:
				print ('-'*60 + "\nRESUME Request Received\n" + '-'*60)
				self.casting = True
		# Process PLAY request
		elif requestType == self.PLAY:
			if (not self.cam) and self.state != self.INIT: #if self.state == self.READY or self.state == self.CASTING:
				print '-'*60 + "\nPLAY Request Received\n" + '-'*60
				self.cam = True
				# Create a new socket for RTP/UDP
				self.clientInfo["rtpSocket_cam"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
				self.clientInfo["rtpSocket_sound"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                                ttl = struct.pack('b', 1)
                                self.clientInfo["rtpSocket_cam"].setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
                                self.clientInfo["rtpSocket_sound"].setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
				self.replyRtsp(self.OK_200, seq[0])
				if dbug:
					print '-'*60 + "\nSequence Number ("+ seq[0] + ")\nReplied to client\n" + '-'*60

				# Create a new thread and start sending RTP packets
				self.clientInfo['event_cam'] = threading.Event()
				self.clientInfo['cam_worker']= threading.Thread(target=self.send_cam_Rtp)	
				self.clientInfo['sound_worker']= threading.Thread(target=self.send_sound_Rtp)
				self.clientInfo['sound_worker'].start()
				self.clientInfo['cam_worker'].start()

		# Process RESUME request
			elif not self.cam:
				print '-'*60 + "\nRESUME Request Received\n" + '-'*60
				self.cam = True

		# Process PAUSE request
		elif requestType == self.PAUSE_CAM:
			if self.cam: 
				print '-'*60 + "\nPAUSE CAM Request Received\n" + '-'*60
				self.state = self.PAUSE_CAM
				self.clientInfo['event_cam'].set()
				self.replyRtsp(self.OK_200, seq[0])
				self.cam = False
		elif requestType == self.PAUSE_SCN:
			if self.casting:
				print '-'*60 + "\nPAUSE SCN Request Received\n" + '-'*60
				self.state = self.PAUSE_SCN
				self.clientInfo['event_scn'].set()
				self.replyRtsp(self.OK_200, seq[0])
				self.casting = False	

		# Process TEARDOWN request
		elif requestType == self.TEARDOWN:
			print '-'*60 + "\nTEARDOWN Request Received\n" + '-'*60
			if self.cam:
				self.clientInfo['event_cam'].set()
				del self.clientInfo['videoStream']
			if self.casting:
				self.clientInfo['event_scn'].set()

			self.replyRtsp(self.OK_200, seq[0])

			# Close the RTP socket
			self.clientInfo['rtpSocket_cam'].close()
			self.clientInfo['rtpSocket_sound'].close()
			self.clientInfo['rtpSocket_scn'].close()
		elif requestType == "MESSAGE":
			print "message received: "
			print request[1][1:]


	def send_cam_Rtp(self):
		"""Send RTP packets over UDP."""

		counter = 0
		threshold = 10
		while True:
			jit = math.floor(random.uniform(-13,5.99))
			jit = jit / 1000

			self.clientInfo['event_cam'].wait(0.05 + jit)
			jit = jit + 0.020
				# Stop sending if request is PAUSE or TEARDOWN
			if self.clientInfo['event_cam'].isSet():
				break

			data = self.clientInfo['videoStream'].nextFrame()
			#print '-'*60 + "\ndata from nextFrame():\n" + data + "\n"
			if data:
				frameNumber = (self.clientInfo['videoStream'].frameNbr())%256
				try:
					#address = 127.0.0.1 #self.clientInfo['rtspSocket'][0][0]
					#port = '25000' #int(self.clientInfo['rtpPort'])

					#print '-'*60 + "\nmakeRtp:\n" + self.makeRtp(data,frameNumber)
					#print '-'*60

					#address = self.clientInfo['rtspSocket'][1]   #!!!! this is a tuple object ("address" , "")

					port = int(self.clientInfo['rtpPort_cam'])
                                        multicast_group = '224.3.29.71'
					prb = math.floor(random.uniform(1,100))
					if prb > 5.0:
						self.clientInfo['rtpSocket_cam'].sendto(self.makeRtp(data, frameNumber),(multicast_group, port))
						counter += 1
						time.sleep(0.02)
				except:
					print "Connection Error"
					print '-'*60
					traceback.print_exc(file=sys.stdout)
					print '-'*60

	def send_sound_Rtp(self):
		"""Send RTP packets over UDP."""
		frameNumber = 0
		counter = 0
		threshold = 10
		while True:
			jit = math.floor(random.uniform(-13,5.99))
			jit = jit / 1000

			self.clientInfo['event_cam'].wait(0.05 + jit)
			jit = jit + 0.020

			# Stop sending if request is PAUSE or TEARDOWN
			if self.clientInfo['event_cam'].isSet():
				break

			# data = self.clientInfo['videoStream'].nextFrame()
			data = streami.read(CHUNK)
			# data = bytes(" "*1023 + "a")
			#print '-'*60 + "\ndata from nextFrame():\n" + data + "\n"
			if data:
			# if True:
				# frameNumber = self.clientInfo['videoStream'].frameNbr()
				frameNumber = (frameNumber + 1)%256
				try:
					#address = 127.0.0.1 #self.clientInfo['rtspSocket'][0][0]
					#port = '25000' #int(self.clientInfo['rtpPort'])

					#print '-'*60 + "\nmakeRtp:\n" + self.makeRtp(data,frameNumber)
					#print '-'*60

					#address = self.clientInfo['rtspSocket'][1]   #!!!! this is a tuple object ("address" , "")

					port = int(self.clientInfo['rtpPort_sound'])

					prb = math.floor(random.uniform(1,100))
					if prb > 0.0:
                                                multicast_group = '224.3.29.71'
						self.clientInfo['rtpSocket_sound'].sendto(self.makeRtp(data, frameNumber),(multicast_group, port))
						counter += 1
						if dbug:
							print "sent number:", frameNumber
						# time.sleep(jit)
				except:
					print "Connection Error"
					print '-'*60
					traceback.print_exc(file=sys.stdout)
					print '-'*60

	def makeRtp(self, payload, frameNbr):
		"""RTP-packetize the video data."""
		version = 2
		padding = 0
		extension = 0
		cc = 0
		marker = 0
		pt = 26 # MJPEG type
		seqnum = frameNbr
		ssrc = 0

		rtpPacket = RtpPacket()

		rtpPacket.encode(version, padding, extension, cc, seqnum, marker, pt, ssrc, payload)

		return rtpPacket.getPacket()
		
	def sendScnRtp(self):
		"""Send RTP packets over UDP."""

		counter = 0
		sct = mss.mss()
		sct.compression_level = 2
		frameNumber =0
		while True:

			jit = math.floor(random.uniform(-13,5.99))
			jit = jit / 1000

			self.clientInfo['event_scn'].wait(0.05 + jit)
			jit = jit + 0.020

			# Stop sending if request is PAUSE or TEARDOWN
			if self.clientInfo['event_scn'].isSet():
				break

			im = sct.grab(self.mon)
			img = Image.frombytes('RGB', im.size, im.bgra, 'raw', 'BGRX').resize(self.screenSize, Image.ANTIALIAS)
			img_io = BytesIO()
			img.save(img_io, 'JPEG', quality=50)
			img_io.seek(0)
			data = img_io.getvalue()
			#print(len(data))
                        multicast_group = '224.3.29.71'
			if data:

				try:
					frameNumber = (frameNumber+1)%256
					#print("frame...",frameNumber)
					port = int(self.clientInfo['rtpPort_scn'])

					prb = math.floor(random.uniform(1,100))
					if prb > 5.0:
						self.clientInfo['rtpSocket_scn'].sendto(self.makeRtp(data, frameNumber),(multicast_group,port))
						counter += 1
						time.sleep(jit)
				except:
					print ("Connection Error")
					print ('-'*60)
					traceback.print_exc(file=sys.stdout)
					print ('-'*60)

				
	def replyRtsp(self, code, seq):
		"""Send RTSP reply to the client."""
		if code == self.OK_200:
			#print "200 OK"
			reply = 'RTSP/1.0 200 OK\nCSeq: ' + seq + '\nSession: ' + str(self.clientInfo['session'])
			connSocket = self.clientInfo['rtspSocket'][0]
			connSocket.send(reply)

		# Error messages
		elif code == self.FILE_NOT_FOUND_404:
			print "404 NOT FOUND"
		elif code == self.CON_ERR_500:
			print "500 CONNECTION ERROR"







def playAudio(self):
		if self.state == self.READY:
			# Create a new thread to listen for RTP packets for audio
			print "Playing Audio"
			threading.Thread(target=self.listenRtpAud).start()
			self.speakEvent = threading.Event()
			self.speakEvent.clear()
			self.sendRtspRequest(self.SPREAK)
