import pyaudio
import wave
import time
import io
import struct 
from Tkinter import *
import tkMessageBox
from PIL import Image, ImageTk, ImageFile
import socket, threading, sys, traceback, os
from RtpPacket import RtpPacket

dbug = False

p = pyaudio.PyAudio()
CHUNK = 1
FORMAT = pyaudio.paInt32
CHANNELS = 1
RATE = 44100

FRAME_WIDTH = 50
FRAME_HEIGHT = 400

streamo = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                output=True)

CACHE_FILE_NAME = "cache-"
CACHE_FILE_EXT = ".jpg"

class Client:
	INIT = 0
	READY = 1
	PLAYING = 2
	CASTING = 3	
	CASTING_AND_PLAYING = 4
	state = INIT

	SETUP = 0
	PLAY = 1
	PAUSE_CAM = 2
	TEARDOWN = 3
	CAST = 4
	PAUSE_SCN = 5


	counter = 0
	# Initiation..
	def __init__(self, master, serveraddr, serverport, rtpport):
		self.master = master
		self.master.protocol("WM_DELETE_WINDOW", self.handler)
		self.createWidgets()
		self.serverAddr = serveraddr
		self.serverPort = int(serverport)
		self.rtpPort_cam = int(rtpport)
		self.rtpPort_sound = int(rtpport) + 1
		self.rtpPort_scn = int(rtpport) + 2
		self.rtspSeq = 0
		self.sessionId = 0
		self.requestSent = -1
		self.teardownAcked = 0
		self.connectToServer()
		self.frameNbr_cam = 0
		self.screenSize = (450, 250)
		self.frameNbr_sound = 0
		self.frameNbr_scn = 0
		self.casting = False
		self.cam = False
		self.rtpSocket_sound = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
		self.rtpSocket_cam = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
		self.rtpSocket_scn = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
		ImageFile.LOAD_TRUNCATED_IMAGES = True

	def createWidgets(self):
		"""Build GUI."""
		# Create Setup button
		self.setup = Button(self.master, width=20, padx=3, pady=3)
		self.setup["text"] = "Setup"
		self.setup["command"] = self.setupMovie
		self.setup.grid(row=1, column=0, padx=2, pady=2)

		# Create Play button
		self.start = Button(self.master, width=20, padx=3, pady=3)
		self.start["text"] = "Play"
		self.start["command"] = self.playMovie
		self.start.grid(row=1, column=1, padx=2, pady=2)

		# Create Pause button
		self.pause = Button(self.master, width=20, padx=3, pady=3)
		self.pause["text"] = "Pause"
		self.pause["command"] = self.pauseMovie
		self.pause.grid(row=1, column=2, padx=2, pady=2)

		# Create Teardown button
		self.teardown = Button(self.master, width=20, padx=3, pady=3)
		self.teardown["text"] = "Teardown"
		self.teardown["command"] =  self.exitClient
		self.teardown.grid(row=1, column=3, padx=2, pady=2)

		self.teardown = Button(self.master, width=20, padx=3, pady=3)
		self.teardown["text"] = "Cast"
		self.teardown["command"] =  self.castScreen
		self.teardown.grid(row=1, column=4, padx=2, pady=2)

		self.teardown = Button(self.master, width=20, padx=3, pady=3)
		self.teardown["text"] = "Stop Cast"
		self.teardown["command"] =  self.pauseCastScreen
		self.teardown.grid(row=1, column=5, padx=2, pady=2)

		#Create message text box
		self.msgbox = Entry(self.master, bd=1, bg="white" , width=40 , highlightbackground="#bebebe", highlightthickness=1)
		self.msgbox.grid(row=2, column=0, padx=2,pady=2, columnspan=2)

		#send message btn
		self.sendmsg = Button(self.master, width=20, padx=3, pady=3)
		self.sendmsg['text'] = "Send"
		self.sendmsg['command'] = self.sendmsgtext
		self.sendmsg.grid(row=2, column=2, padx=2,pady=2)

		# Create a label to display the movie
		self.label = Label(self.master, height=19)
		self.label.grid(row=0, column=0, columnspan=4, padx=5, pady=5)

		self.cast = Label(self.master, height=19)
		self.cast.grid(row=0, column=4, columnspan=4, padx=5, pady=5)

	def setupMovie(self):
		"""Setup button handler."""
		if self.state == self.INIT:
			self.sendRtspRequest(self.SETUP)

	def exitClient(self):
		"""Teardown button handler."""
		self.sendRtspRequest(self.TEARDOWN)
		#self.handler()
		self.master.destroy() # Close the gui window
		os.remove(CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT) # Delete the cache image from video
		rate = float(self.counter/self.frameNbr_cam)
		print '-'*60 + "\nRTP Packet Loss Rate :" + str(rate) +"\n" + '-'*60
		sys.exit(0)

	def pauseMovie(self):
		"""Pause button handler."""
		if self.cam:
			self.sendRtspRequest(self.PAUSE_CAM)
	def pauseCastScreen(self):
		"""Pause button handler."""
		if self.casting:
			self.sendRtspRequest(self.PAUSE_SCN)
	def playMovie(self):
		"""Play button handler."""
		if not self.cam:
			# Create a new thread to listen for RTP packets
			if dbug:
				print "Playing Movie"
			threading.Thread(target=self.listenRtp_sound).start()
			threading.Thread(target=self.listenRtp_cam).start()
			self.playEvent = threading.Event()
			self.playEvent.clear()
			self.sendRtspRequest(self.PLAY)

	def castScreen(self):
		"""Play button handler."""
		if not self.casting:
			# Create a new thread to listen for RTP packets
			print ("casting")
			threading.Thread(target=self.listenRtp_scn).start()
			self.screenEvent = threading.Event()
			self.screenEvent.clear()
			self.sendRtspRequest(self.CAST)

	def sendmsgtext(self):
		message = self.msgbox.get()
		self.sendRtspRequest(-1, message)

	def listenRtp_cam(self):
		while True:
			try:
				data,addr = self.rtpSocket_cam.recvfrom(25600)

				if data:
					rtpPacket = RtpPacket()
					rtpPacket.decode(data)
					if dbug:
						print "||Received Cam Rtp Packet #" + str(rtpPacket.seqNum()) + "|| "

					try:
						if ((self.frameNbr_cam + 1)%256) != rtpPacket.seqNum():
							self.counter += 1
							if dbug:
								print '!'*60 + "\nPACKET LOSS\n" + '!'*60
						currFrameNbr = rtpPacket.seqNum()
						#version = rtpPacket.version()
					except:
						print "seqNum() error"
						print '-'*60
						traceback.print_exc(file=sys.stdout)
						print '-'*60

					if (currFrameNbr > self.frameNbr_cam) or (self.frameNbr_cam - currFrameNbr > 240): # Discard the late packet
						self.frameNbr_cam = currFrameNbr
						if dbug:
							print len(rtpPacket.getPayload())
						self.updateMovie(rtpPacket.getPayload())

			except:
				# Stop listening upon requesting PAUSE or TEARDOWN
				print "Didn`t receive data!"
				if self.playEvent.isSet():
					break

				# Upon receiving ACK for TEARDOWN request,
				# close the RTP socket
				if self.teardownAcked == 1:
					self.rtpSocket_cam.shutdown(socket.SHUT_RDWR)
					self.rtpSocket_cam.close()
					break


	def listenRtp_sound(self):
		while True:
			try:
				data,addr = self.rtpSocket_sound.recvfrom(20480)

				if data:
					rtpPacket = RtpPacket()
					rtpPacket.decode(data)
					if dbug:
						print "||Received sound Rtp Packet #" + str(rtpPacket.seqNum()) + "|| "

					try:
						if ((self.frameNbr_sound + 1)%256) != rtpPacket.seqNum():
							self.counter += 1
							if dbug:
								print '!'*60 + "\nPACKET LOSS\n" + '!'*60
						currFrameNbr = rtpPacket.seqNum()
						#version = rtpPacket.version()
					except:
						print "seqNum() error"
						print '-'*60
						traceback.print_exc(file=sys.stdout)
						print '-'*60

					if (currFrameNbr > self.frameNbr_sound) or (self.frameNbr_sound - currFrameNbr > 240): # Discard the late packet
						self.frameNbr_sound = currFrameNbr
						# self.updateMovie(self.writeFrame(rtpPacket.getPayload()))
						# print "got a packet"
						streamo.write(rtpPacket.getPayload())
						# print "payload", rtpPacket.getPayload()
					else:
						print "discarded late"

			except:
				# Stop listening upon requesting PAUSE or TEARDOWN
				print "Didn`t receive data!"
				if self.playEvent.isSet():
					break

				# Upon receiving ACK for TEARDOWN request,
				# close the RTP socket
				if self.teardownAcked == 1:
					self.rtpSocket_sound.shutdown(socket.SHUT_RDWR)
					self.rtpSocket_sound.close()
					break
	def writeFrame(self, data):
		cachename = CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT
		try:
			file = open(cachename, "wb")
		except:
			print "file open error"
		try:
			file.write(data)
		except:
			print "file write error"
		file.close()
		return cachename

	def listenRtp_scn(self):
		while True:
			try:
				data,addr = self.rtpSocket_scn.recvfrom(60960)
				# print("data is here",data)
				if data:
					rtpPacket = RtpPacket()
					rtpPacket.decode(data)
					if dbug:
						print( "||Received Scn Rtp Packet #" + str(rtpPacket.seqNum()) + "|| ")

					try:
						if ((self.frameNbr_scn + 1)%256) != rtpPacket.seqNum():
							self.counter += 1
							if dbug:
								print ('!'*60 + "\nPACKET LOSS\n" + '!'*60)
						currFrameNbr = rtpPacket.seqNum()
						#version = rtpPacket.version()
					except:
						print( "seqNum() error")
						print ('-'*60)
						traceback.print_exc(file=sys.stdout)
						print ('-'*60)

					if (currFrameNbr > self.frameNbr_scn) or (self.frameNbr_scn - currFrameNbr > 240): # Discard the late packet
						self.frameNbr_scn = currFrameNbr
						self.updateScn(self.writeFrame(rtpPacket.getPayload()))
			except:
				# Stop listening upon requesting PAUSE or TEARDOWN
				print "Didn`t receive data!"
				if self.screenEvent.isSet():
					break

				# Upon receiving ACK for TEARDOWN request,
				# close the RTP socket
				if self.teardownAcked == 1:
					self.rtpSocket_scn.shutdown(socket.SHUT_RDWR)
					self.rtpSocket_scn.close()
					break


	def updateMovie(self, data):
		"""Update the image file as video frame in the GUI."""
		try:
			pil_bytes = io.BytesIO(data)
			photo = ImageTk.PhotoImage(Image.open(pil_bytes))
		except:
			print "photo error"
			print '-'*60
			traceback.print_exc(file=sys.stdout)
			print '-'*60
		self.label.configure(image = photo, height=FRAME_HEIGHT)
		self.label.image = photo

	def updateScn(self, image_file):
		"""Update the image file as video frame in the GUI."""
		try:
			photo = ImageTk.PhotoImage(Image.open(image_file).resize(self.screenSize, Image.ANTIALIAS))
		except:
			print "photo error"
			print '-'*60
			traceback.print_exc(file=sys.stdout)
			print '-'*60
		self.cast.configure(image = photo, height=FRAME_HEIGHT)
		self.cast.image = photo


	def connectToServer(self):
		"""Connect to the Server. Start a new RTSP/TCP session."""
		self.rtspSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.rtspSocket.connect((self.serverAddr, self.serverPort))
		except:
			tkMessageBox.showwarning('Connection Failed', 'Connection to \'%s\' failed.' %self.serverAddr)

	def sendRtspRequest(self, requestCode, message=""):
		"""Send RTSP request to the server."""

		# Setup request
		if requestCode == self.SETUP and self.state == self.INIT:
			threading.Thread(target=self.recvRtspReply).start()
			# Update RTSP sequence number.
			# ...
			self.rtspSeq = 1

			# Write the RTSP request to be sent.
			# request = ...
			request = "SETUP " + "SERVER" + "\n" + str(self.rtspSeq) + "\n" + " RTSP/1.0 RTP/UDP " + str(self.rtpPort_cam)

			self.rtspSocket.send(request)
			# self.state = self.READY
			# Keep track of the sent request.
			# self.requestSent = ...
			self.requestSent = self.SETUP

		# Play request
		elif requestCode == self.PLAY and self.state == self.READY:
			# Update RTSP sequence number.
			# ...
			self.rtspSeq = self.rtspSeq + 1
			# Write the RTSP request to be sent.
			# request = ...
			request = "PLAY " + "\n" + str(self.rtspSeq)

			self.rtspSocket.send(request)
			print '-'*60 + "\nPLAY request sent to Server...\n" + '-'*60
			# Keep track of the sent request.
			# self.requestSent = ...
			self.requestSent = self.PLAY

		elif requestCode == self.CAST and self.state == self.READY:
			# Update RTSP sequence number.
			# ...
			self.rtspSeq = self.rtspSeq + 1
			# Write the RTSP request to be sent.
			# request = ...
			request = "CAST " + "\n" + str(self.rtspSeq)

			self.rtspSocket.send(request)
			print '-'*60 + "\nCAST request sent to Server...\n" + '-'*60
			# Keep track of the sent request.
			# self.requestSent = ...
			self.requestSent = self.CAST

		# Pause request
		elif requestCode == self.PAUSE_CAM and self.cam:
			# Update RTSP sequence number.
			# ...
			self.rtspSeq = self.rtspSeq + 1
			# Write the RTSP request to be sent.
			# request = ...
			request = "PAUSE_CAM " + "\n" + str(self.rtspSeq)
			self.rtspSocket.send(request)
			print '-'*60 + "\nPAUSE_CAM request sent to Server...\n" + '-'*60
			# Keep track of the sent request.
			# self.requestSent = ...
			self.requestSent = self.PAUSE_CAM

		elif requestCode == self.PAUSE_SCN and self.casting:
			# Update RTSP sequence number.
			# ...
			self.rtspSeq = self.rtspSeq + 1
			# Write the RTSP request to be sent.
			# request = ...
			request = "PAUSE_SCN " + "\n" + str(self.rtspSeq)
			self.rtspSocket.send(request)
			print '-'*60 + "\nPAUSE_SCN request sent to Server...\n" + '-'*60
			# Keep track of the sent request.
			# self.requestSent = ...
			self.requestSent = self.PAUSE_SCN

		elif requestCode == -1:
			self.rtspSeq = self.rtspSeq + 1
			request = "MESSAGE " + "\n" + str(self.rtspSeq) + " " + message
			self.rtspSocket.send(request)

		# Teardown request
		elif requestCode == self.TEARDOWN and not self.state == self.INIT:
			# Update RTSP sequence number.
			# ...
			self.rtspSeq = self.rtspSeq + 1
			# Write the RTSP request to be sent.
			# request = ...
			request = "TEARDOWN " + "\n" + str(self.rtspSeq)
			self.rtspSocket.send(request)
			print '-'*60 + "\nTEARDOWN request sent to Server...\n" + '-'*60
			# Keep track of the sent request.
			# self.requestSent = ...
			self.requestSent = self.TEARDOWN

		else:
			return

		# Send the RTSP request using rtspSocket.
		# ...

#		print '\nData sent:\n' + request

	def recvRtspReply(self):
		"""Receive RTSP reply from the server."""
		while True:
			reply = self.rtspSocket.recv(1024)

			if reply:
				self.parseRtspReply(reply)

			# Close the RTSP socket upon requesting Teardown
			if self.requestSent == self.TEARDOWN:
				self.rtspSocket.shutdown(socket.SHUT_RDWR)
				self.rtspSocket.close()
				break

	def parseRtspReply(self, data):
		print "Parsing Received Rtsp data..."

		"""Parse the RTSP reply from the server."""
		lines = data.split('\n')
		seqNum = int(lines[1].split(' ')[1])

		# Process only if the server reply's sequence number is the same as the request's
		if seqNum == self.rtspSeq:
			session = int(lines[2].split(' ')[1])
			# New RTSP session ID
			if self.sessionId == 0:
				self.sessionId = session

			# Process only if the session ID is the same
			if self.sessionId == session:
				if int(lines[0].split(' ')[1]) == 200:
					if self.requestSent == self.SETUP:
						# Update RTSP state.
						print "Updating RTSP state..."
						# self.state = ...
						self.state = self.READY
						# Open RTP port.
						#self.openRtpPort()
						print "Setting Up RtpPort for Video Stream"
						self.openRtpPort()
					elif self.requestSent == self.PLAY:
						self.cam = True
						print '-'*60 + "\nClient is PLAYING...\n" + '-'*60
					elif self.requestSent == self.CAST:
						self.casting = True
						print '-'*60 + "\nClient is CASTING...\n" + '-'*60
					elif self.requestSent == self.PAUSE_CAM: 
						self.state = self.READY
						self.cam = False
						# The play thread exits. A new thread is created on resume.
						self.playEvent.set()
					elif self.requestSent == self.PAUSE_SCN: 
						self.state = self.READY
						self.casting = False
						# The play thread exits. A new thread is created on resume.
						self.screenEvent.set()
					elif self.requestSent == self.TEARDOWN:
						# self.state = ...
						# Flag the teardownAcked to close the socket.
						self.teardownAcked = 1

	def openRtpPort(self):
		"""Open RTP socket binded to a specified port."""
		self.rtpSocket_cam.settimeout(0.5)
		self.rtpSocket_sound.settimeout(0.5)
		self.rtpSocket_scn.settimeout(0.5)
                multicast_group = '224.3.29.71'
                group = socket.inet_aton(multicast_group)
                mreq = struct.pack('4sL', group, socket.INADDR_ANY)
                
		try:
			#self.rtpSocket.connect(self.serverAddr,self.rtpPort)
			self.rtpSocket_cam.bind(('',self.rtpPort_cam))  
			self.rtpSocket_sound.bind(('',self.rtpPort_sound))
			self.rtpSocket_scn.bind(('',self.rtpPort_scn))
                        self.rtpSocket_cam.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
                        self.rtpSocket_sound.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
                        self.rtpSocket_scn.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
			#self.rtpSocket.listen(5)
			print "Bind RtpPort Success"

		except:
			tkMessageBox.showwarning('Connection Failed', 'Connection to rtpServer failed...')


	def handler(self):
		"""Handler on explicitly closing the GUI window."""
		self.pauseMovie()
		if tkMessageBox.askokcancel("Quit?", "Are you sure you want to quit?"):
			self.exitClient()
		else: 
			print "Playing Movie"
			threading.Thread(target=self.listenRtp).start()
			#self.playEvent = threading.Event()
			#self.playEvent.clear()
			self.sendRtspRequest(self.PLAY)
