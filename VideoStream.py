import struct
import cv2
from PIL import Image
import io

dbug = False

class VideoStream:
        def __init__(self):
		self.camera_index = 0
                capture = 1
                try:
		        if capture == 1:
			        self.cam = cv2.VideoCapture(self.camera_index)
			        self.cam.set(cv2.CAP_PROP_FPS, 16)
			        #print '-'*60 +  "\nVideo file : |" + filename +  "| read\n" + '-'*60
                except:
		        print "read " + " error"
		        raise IOError
                self.frameNum = 0
        
	def nextFrame(self):
	        """Get next frame."""
                ret_val, img = self.cam.read()
                c = cv2.waitKey(1)
		# Get the framelength from the first 5 bytes
		#data_ints = struct.unpack('<' + 'B'*len(data),data)
                if (c == "n"): #in "n" key is pressed while the popup window is in focus
                        self.camera_index += 1 #try the next camera index
                        self.cam = cv2.VideoCapture(self.camera_index)
                        self.cam.set(cv2.CAP_PROP_FPS, 16)
                        if not self.cam: #if the next camera index didn't work, reset to 0.
                                self.camera_index = 0
                                self.cam = cv2.VideoCapture(self.camera_index)
                                self.cam.set(cv2.CAP_PROP_FPS, 16)
		
                cv2_im = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                pil_im = Image.fromarray(cv2_im)
                b = io.BytesIO()
                pil_im.save(b, 'jpeg')
                im_bytes = b.getvalue()
                if ret_val:
                        framelength = len(im_bytes)#int(data)#final_data_int/8  # xx bytes
                        self.frameNum += 1
                        if dbug:
                        	print '-'*10 + "\nNext Frame (#" + str(self.frameNum) + ") length:" + str(framelength) + "\n" + '-'*10
                        return im_bytes

			# Read the current frame
			#frame = self.file.read(framelength)
			#if len(frame) != framelength:
			#	raise ValueError('incomplete frame data')
			#print "frame length"
			#print len(frame)
			#if not (data.startswith(b'\xff\xd8') and data.endswith(b'\xff\xd9')):
			#	raise ValueError('invalid jpeg')
        
	def frameNbr(self):
		"""Get frame number."""
		return self.frameNum
	def __del__(self):
		self.cam.release()

