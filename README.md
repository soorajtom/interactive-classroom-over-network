# interactive-classroom-over-network

## Introduction
This project is aimed at making a python app that can be used to have lectures taken remotely, while allowing the students to interact with the lecturer in between. The project uses Real-time Transport Protocol (RTP) and Real Time Streaming Protocol (RTSP).

## Team members
- Axel James 
- Libin N George
- Sooraj Tom

## Installing dependencies
```bash
$ sudo apt-get install portaudio19-dev
$ pip install -r requirements.txt
```

## Running the server
```bash
$ python StartServer.py port_number
```

## Running the client
```bash
$ python launcher.py host_address host_port rtp_port
```